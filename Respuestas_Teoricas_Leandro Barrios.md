## Parte Teórica

 **A ) Define muy brevemente los tres problemas principales del desarrollo de
software que Docker ayuda a resolver**.


- Diferencias de entorno: Docker proporciona entornos de desarrollo consistentes entre equipos de desarrollo, evitando problemas de "funciona en mi máquina".

- Gestión de dependencias: Permite encapsular las dependencias del software en contenedores, lo que facilita su distribución y asegura que todas las partes del sistema funcionen correctamente.

- Escalabilidad y portabilidad: Docker facilita la creación y gestión de entornos escalables y portátiles, lo que permite ejecutar aplicaciones en cualquier entorno sin preocuparse por la infraestructura

**B ) ¿Qué comandos usarías para listar todos los contenedores en ejecución, los
detenidos y con cuál pueden borrar de una sola vez, todos los contenedores
que están detenidos?**



	$ docker ps

	$ docker ps -a
	
	$ docker container prune


**C ) ¿Con tus palabras y en forma muy breve, cuál es la diferencia entre bind
mounts y volúmenes en Docker?**

Un bind mount en Docker es como conectar un directorio o archivo de tu computadora al interior de un contenedor. Imagina que estás compartiendo una carpeta con tu amigo: cualquier cambio que hagas en esa carpeta se verá instantáneamente tanto en tu computadora como en la de tu amigo.

Por otro lado, un volumen en Docker es como una caja especial para guardar cosas que necesitas para tus contenedores. Puedes guardar archivos, bases de datos o cualquier otra cosa que tus contenedores necesiten en esta caja. Agregando que de esta manera es segura y eficiente.
