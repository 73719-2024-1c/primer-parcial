
# 1) Ejercicio versionado GIT
**Se crea el directorio "Practica_GIT":**

     [lbarrios@debian ~]$ mkdir Practica_GIT

**No posicionamos en el directorio creado:**

	 [lbarrios@debian ~]$ cd Practica_GIT/

**Se setea las congiguraciones iniciales**

	[lbarrios@debian Practica_GIT]$ git config --global user.name "Leandro Barrios"

	[lbarrios@debian Practica_GIT]$ git config --global user.email "leandro.oscar.barrios@istea.com.ar"

**Inicializamos el repositorio de "Practica_GIT"**

	[lbarrios@debian Practica_GIT]$ git init

**Verificamos que esten los archivos cargados:**

	[lbarrios@debian Practica_GIT]$ ls

	app.js  Dockerfile  package.json  Respuestas_Teoricas_Nombre_Apellido.docx

**Se agregan los archivos nuevos o editados:**

	[lbarrios@debian Practica_GIT]$ git add .

**Se confirman los cambios y se pone el mensaje "Primer commit"**

	[lbarrios@debian Practica_GIT]$ git commit -m "Primer commit"

    [master (commit-raíz) 69ac02c] Primer commit
    
    4 files changed, 45 insertions(+)
    
    create mode 100644 Dockerfile
    
    create mode 100644 Respuestas_Teoricas_Nombre_Apellido.docx
    
    create mode 100644 app.js
    
    create mode 100644 package.json

**Se agrega el repositorio remoto al repositorio local**

	[lbarrios@debian Practica_GIT (master)]$ git remote add origin https://gitlab.com/73719-2024-1c/primer-parcial.git

**Se envian los commits locales a la rama master en el repositorio remoto:**

	[lbarrios@debian Practica_GIT (master)]$ git push -u origin master

**Luego de ejecutar este comando se solicitara las crendenciales de gitlab *(Este paso se repite en cada push)***

**Creamos las ramas: dev, TESTING y Staging**

	[lbarrios@debian Practica_GIT (master)]$ git branch dev

	[lbarrios@debian Practica_GIT (master)]$ git branch TESTING

	[lbarrios@debian Practica_GIT (master)]$ git branch Staging

**Cambiamos a la rama "dev"**

	[lbarrios@debian Practica_GIT (master)]$ git checkout dev

	Cambiado a rama 'dev'

**Una vez en la rama dev, editamos el archivo "package.json" en la sección de versión 1.0.0 por 1.0.1**

	[lbarrios@debian Practica_GIT (dev)]$ nano package.json

**Se agregaga la modificacion:**

	[lbarrios@debian Practica_GIT (dev)]$ git add .

**Se confirman los cambios y se pone el mensaje "V1.0.1"**

	[lbarrios@debian Practica_GIT (dev)]$ git commit -m "V1.0.1"

	[dev 22aa122] V1.0.1

	1 file changed, 1 insertion(+), 1 deletion(-)

**Se envia el commit local a la rama dev en el repositorio remoto:**

	[lbarrios@debian Practica_GIT (dev)]$ git push origin dev

**Cambiamos a la rama "TESTING"**

	[lbarrios@debian Practica_GIT (dev)]$ git checkout TESTING

	Cambiado a rama 'TESTING'

**Se hace un Merge de la rama dev**:

	[lbarrios@debian Practica_GIT (TESTING)]$ git merge dev

	Actualizando 69ac02c..22aa122

	Fast-forward

	package.json | 2 +-

	1 file changed, 1 insertion(+), 1 deletion(-)

**Se envia el commit local a la rama TESTING en el repositorio remoto:**

	[lbarrios@debian Practica_GIT (TESTING)]$ git push origin TESTING

**Cambiamos a la rama "Staging"**

	[lbarrios@debian Practica_GIT (TESTING)]$ git checkout Staging

	Cambiado a rama 'Staging'

**Se hace un Merge de la rama TESTING**:

	[lbarrios@debian Practica_GIT (Staging)]$ git merge TESTING

	Actualizando 69ac02c..22aa122

	Fast-forward

	package.json | 2 +-

	1 file changed, 1 insertion(+), 1 deletion(-)

**Cambiamos a la rama "dev"**

	[lbarrios@debian Practica_GIT (Staging)]$ git checkout dev

	Cambiado a rama 'dev'

**Una vez en la rama dev, editamos el archivo "package.json" en la sección de versión 1.0.1 por 1.0.2**

	[lbarrios@debian Practica_GIT (dev)]$ nano package.json

**Se agregaga la modificacion:**

	[lbarrios@debian Practica_GIT (dev)]$ git add .

**Se confirman los cambios y se pone el mensaje "V1.0.2"**

	[lbarrios@debian Practica_GIT (dev)]$ git commit -m "V1.0.2"

	[dev 61b4670] V1.0.2

	1 file changed, 1 insertion(+), 1 deletion(-)

**Se envia el commit local a la rama dev en el repositorio remoto:**

	[lbarrios@debian Practica_GIT (dev)]$ git push origin dev

**Cambiamos a la rama "master"**

	[lbarrios@debian Practica_GIT (dev)]$ git checkout master

	Cambiado a rama 'master'

	Tu rama está actualizada con 'origin/master'.

**Se hace un Merge de la rama Staging**:

	[lbarrios@debian Practica_GIT (master)]$ git merge Staging

	Actualizando 69ac02c..22aa122

	Fast-forward

	package.json | 2 +-

	1 file changed, 1 insertion(+), 1 deletion(-)

**Se envia el commit local a la rama master en el repositorio remoto:**

[lbarrios@debian Practica_GIT (master)]$ git push origin master

**Cambiamos a la rama "TESTING"**

	[lbarrios@debian Practica_GIT (master)]$ git checkout TESTING

	Cambiado a rama 'TESTING'

**Se hace un Merge de la rama dev**:

	[lbarrios@debian Practica_GIT (TESTING)]$ git merge dev

	Actualizando 22aa122..61b4670

	Fast-forward

	package.json | 2 +-

	1 file changed, 1 insertion(+), 1 deletion(-)

**Se envia el commit local a la rama TESTING en el repositorio remoto:**

	[lbarrios@debian Practica_GIT (TESTING)]$ git push origin TESTING

**Cambiamos a la rama "dev"**

	[lbarrios@debian Practica_GIT (TESTING)]$ git checkout dev

	Cambiado a rama 'dev'

**Una vez en la rama dev, editamos el archivo "package.json" en la sección de versión 1.0.2 por 1.0.3**

	[lbarrios@debian Practica_GIT (dev)]$ nano package.json

**Se agregaga la modificacion:**

	[lbarrios@debian Practica_GIT (dev)]$ git add .

**Se confirman los cambios y se pone el mensaje "V1.0.3"**
	
	[lbarrios@debian Practica_GIT (dev)]$ git commit -m "V1.0.3"

	[dev f2cda2f] V1.0.3

	1 file changed, 1 insertion(+), 1 deletion(-)

**Se envia el commit local a la rama dev en el repositorio remoto:**

	[lbarrios@debian Practica_GIT (dev)]$ git push origin dev
	
**Cambiamos a la rama "Staging"**

	[lbarrios@debian Practica_GIT (dev)]$ git checkout Staging

	Cambiado a rama 'Staging'

**Se agregaga la modificacion:**

	[lbarrios@debian Practica_GIT (Staging)]$ git add .

**Se confirman los cambios y se pone el mensaje "V1.0.1"**

	[lbarrios@debian Practica_GIT (Staging)]$ git commit -m "V1.0.1"

**Se envia el commit local a la rama Staging en el repositorio remoto:**

	[lbarrios@debian Practica_GIT (Staging)]$ git push origin Staging



##  Parte Práctica:
**1) Gestión de un contenedor Ubuntu**

[lbarrios@debian ~]$ mkdir Practica_Docker

	[lbarrios@debian Practica_Docker]$ docker run -d --name ubuntu_container ubuntu:20.04

	Unable to find image 'ubuntu:20.04' locally

	20.04: Pulling from library/ubuntu

	d4c3c94e5e10: Pull complete

	Digest: sha256:874aca52f79ae5f8258faff03e10ce99ae836f6e7d2df6ecd3da5c1cad3a912b

	Status: Downloaded newer image for ubuntu:20.04

	5e6f1167adeeee22ccd9b89d4cb8168c995311a82c4bf8b6e7fa0bb75811ae5e

**Se borra el contenedor ya que de lo contrario tirara un error el punto siguiente**
	
	[lbarrios@debian Practica_Docker]$ docker rm ubuntu_container

**Volvemos a crear el contendor pero agregamos el argumento para que sea interactivo**

	[lbarrios@debian ~]$ docker run -d -it --name ubuntu_container ubuntu:20.04

**Ahora ejecutamos el comando docker exec -it ubuntu_container bash para poder ingresar a la consola:**

	[lbarrios@debian ~]$ docker exec -it ubuntu_container bash
	root@139da427427b:/#

**Creamos la carpeta "Primer_Parcial",  y un archivo llamado "examen.txt" dentro**

	root@f3c5605a12dd:/# mkdir Primer_Parcial && touch Primer_Parcial/examen.txt

**Verificamos que se hata creado:**

	root@139da427427b:/# ls Primer_Parcial/
	examen.txt



**2)  Configuración de Nginx**


**Nos ubicamos en el directorio Practica_Docker y luego descargar el archivo “default.conf” (subido al classroom del examen)**

	[lbarrios@debian ~]$ cd Practica_Docker/

**Creamos un volumen llamado "nginx_config"**

	[lbarrios@debian Practica_Docker]$ docker volume create nginx_config

	nginx_config

Levantamos un contenedor Nginx llamado "nginx_container", montando el volumen "nginx_config" en el directorio /etc/nginx/conf.d y puerto 8080

	[lbarrios@debian Practica_Docker]$ docker run -d -p 8080:80 --name nginx_container -v nginx_config:/etc/nginx/conf.d nginx

	Unable to find image 'nginx:latest' locally

	latest: Pulling from library/nginx

	b0a0cf830b12: Pull complete

	8ddb1e6cdf34: Pull complete

	5252b206aac2: Pull complete

	988b92d96970: Pull complete

	7102627a7a6e: Pull complete

	93295add984d: Pull complete

	ebde0aa1d1aa: Pull complete

	Digest: sha256:ed6d2c43c8fbcd3eaa44c9dab6d94cb346234476230dc1681227aa72d07181ee

	Status: Downloaded newer image for nginx:latest

	bdb40cd0aba5831225ba3fdc54959bf4ef9d1f50effdb0ee3f23d0a3f4f3fe79

**Copiar el archivo “default.conf” (subido al classroom del examen) a la ruta “/etc/nginx/conf.d/” del contenedor**

	[lbarrios@debian Practica_Docker]$ docker cp default.conf nginx_container:/etc/nginx/conf.d/

**Ejecutamos el comando "docker exec nginx_container nginx -s reload" para recargar la configuracion**

	[lbarrios@debian Practica_Docker]$ docker exec nginx_container nginx -s reload

	2024/05/05 20:24:26 [notice] 30#30: signal process started